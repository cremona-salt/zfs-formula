# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import zfs with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

{%- set pools = zfs.get('pools', {}) %}

{%- for pool, data in pools.items() %}
{%- if data.config is defined %}
{%- set config = data.config %}
{%- endif %}
{%- if data.properties is defined %}
{%- set properties = data.properties %}
{%- endif %}
{%- if data.filesystem_properties is defined %}
{%- set filesystem_properties = data.filesystem_properties %}
{%- endif %}
{%- set vdevs = data.vdevs %}

zfs-config-mount-{{ pool }}-pool-managed:
  zpool.present:
    - name: {{ pool }}
    - require:
      - sls: {{ sls_package_install }}
    {%- if config is defined %}
    - config:
        {%- for key, val in config.items() %}
        {{ key }}: {{ val }}
        {%- endfor %}
    {%- endif %}
    {%- if properties is defined %}
    - properties:
        {%- for key, val in properties.items() %}
        {{ key }}: {{ val }} 
        {%- endfor %}
    {%- endif %}
    {%- if filesystem_properties is defined %}
    - filesystem_properties:
        {%- for key, val in filesystem_properties.items() %}
        {{ key }}: {{ val }}
        {%- endfor %}
    {%- endif %}
    - layout:
        {%- for vdev, disks in vdevs.items() %}
        - {{ vdev }}:
          {%- for disk in disks %}
          - {{ disk }}
          {%- endfor %}
        {%- endfor %}
{%- endfor %}

{%- set filesystems = zfs.get('filesystems', {}) %}

{%- for filesystem, data in filesystems.items() %}
{%- set pool = data.pool %}
{%- set path = data.path %}
{%- set create_parent = data.get('create_parent', True) %}
{%- if data.properties is defined %}
{%- set properties = data.properties %}
{%- endif %}
{%- if data.cloned_from is defined %}
{%- set cloned_from = data.cloned_from %}
{%- endif %}
zfs-config-mount-{{ pool }}-{{ filesystem }}-filesystem-managed:
  zfs.filesystem_present:
    - name: {{ pool }}/{{ path }}
    - create_parent: {{ create_parent }}
    {%- if properties is defined %}
    - properties:
        {%- for key, val in properties.items() %}
        {{ key }}: {{ val }} 
        {%- endfor %}
    {%- endif %}
    {%- if cloned_from is defined %}
    - cloned_from: {{ cloned_from }}
    {%- endif %}
    - require:
      - zfs-config-mount-{{ pool }}-pool-managed

{%- endfor %}
